package az.ingress.kafka.handler;

import az.ingress.kafka.model.WeatherInfoEvent;

public interface WeatherInfoEventListener {

    void onData(WeatherInfoEvent event);

    void processComplete();
}
